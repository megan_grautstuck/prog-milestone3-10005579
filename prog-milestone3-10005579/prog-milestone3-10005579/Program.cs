﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone3_10005579
{

    public class customer
    {
        public string name;
        public int phone;

        public customer()
        {
            name = "megan";
            phone = 0;
        }
        public customer(string _name, int _phone)
        {
            name = _name;
            phone = _phone;
        }
        public void print ()
        {
            Console.WriteLine(" Are your details correct?");
            Console.WriteLine($" Your name: {name}");
            Console.WriteLine($" Your number: 0{phone}");
        }
    }
    public class Program
    {
        static Dictionary<string, double> dict = new Dictionary<string, double>();

        static void Main(string[] args)
        {
            Console.WriteLine("Hi welcome to the pizzzaaaa shop!!!");
            // intro and retreiving name/number

            Console.WriteLine("What is your name? :D ");
            var custname = Console.ReadLine();

            Console.WriteLine($"thanks {custname}, what is your phone number?");

            var custphone = int.Parse(Console.ReadLine());

            customer cust = new customer(custname, custphone);

            Console.WriteLine($"Thanks {custname} !");
            Console.WriteLine("Press enter to order food!");
            Console.ReadLine();
            Console.Clear();

            // add first pizza+size to dict
            pizzachoice();

            Console.WriteLine("Thankyou, press enter to select your second pizza");
            Console.ReadLine();
            Console.Clear();

            // add second to dict
            pizzachoice();
            Console.WriteLine("Please press enter to add drinks to your order");
            Console.ReadLine();
            Console.Clear();

            // add drinks
            drinks();
            Console.WriteLine("Thanks, press enter to place order");
            Console.ReadLine();

            // check customer details
            Console.Clear();
            cust.print();
            Console.WriteLine("Please press 1 for yes and 2 for no");
            var yesno = int.Parse(Console.ReadLine());
            if (yesno == 1)
            {
              Console.Clear();
              Console.WriteLine($"Here is your order total;");

               foreach (var x in dict)
                {
                   Console.WriteLine($"1x {x.Key} : ${x.Value}");
                
                   Console.WriteLine("_____________________________");

                }

            //if correct details,totals shown and payment recieved = "thankyou"
             ordertotal();
                Console.WriteLine("_____________________");
                Console.WriteLine("Thank you come again!");
                Console.ReadLine();
            }
            else if (yesno == 2)
            {
                Console.Clear();
                Console.WriteLine("Sorry please start over.");
                Console.ReadLine();
                
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Sorry please start over.");
                Console.ReadLine();
            }

        }

        public static void pizzachoice()
        {
            Console.Clear();

            var ham = "ham and pineapple";
            var meat = "meatlovers";
            var chuzb = "cheesy bacon and BBQ";
            var vege = "vegetarian";

            Console.WriteLine("____Pizza menu:____ ");
            Console.WriteLine("                  ");
            Console.WriteLine("1. Ham and pineapple");
            Console.WriteLine("2. Meatlovers");
            Console.WriteLine("3. cheesy bacon 'n' BBQ");
            Console.WriteLine("4. Vegetarian");
            Console.WriteLine("                      ");
            Console.WriteLine("________________________");

            Console.WriteLine("please type the no. of the pizza you would like :) thankyou: ");

            var pizzanum = Console.ReadLine();
            Console.Clear();


            switch (pizzanum)
            {
                case "1":
                    Console.WriteLine("Thanks, one Ham and pineapple coming right up!");
                    pizzasize(ham);
                    break;

                case "2":
                    Console.WriteLine("Thanks, one Meatlovers coming right up !");
                    pizzasize(meat);
                    break;
                case "3":
                    Console.WriteLine("Thanks, one cheesy bacon bbq coming right up !");
                    pizzasize(chuzb);
                    break;
                case "4":
                    Console.WriteLine("Thanks, one Vegetarian coming right up !");
                    pizzasize(vege);
                    break;

                default:
                    Console.WriteLine("The selection was invalid");
                    break;
            }


        }
        public static void pizzasize(string _pizza)
        {

            Console.WriteLine($" What size {_pizza} pizza would you like?");

            double small = 7.50;
            double medium = 10.00;
            double large = 13.00;

            Console.WriteLine($" Small: ${small}");
            Console.WriteLine($" Medium: ${medium}");
            Console.WriteLine($" Large: ${large}");

            var size = Console.ReadLine();
            switch (size)
            {
                case "small":
                    Console.Clear();
                    Console.WriteLine($"You have selected one small {_pizza} pizza ");
                    dict.Add(_pizza, small);

                    break;

                case "medium":
                    Console.Clear();
                    Console.WriteLine($"You have selected one medium {_pizza} pizza ");
                    dict.Add(_pizza, medium);

                    break;

                case "large":
                    Console.Clear();
                    Console.WriteLine($"You have selected one large {_pizza} pizza ");
                    dict.Add(_pizza, large);

                    break;

                default:
                    Console.WriteLine("The selection was invalid");

                    dict.Clear();

                    break;
            }
        }
        public static void ordertotal()
        {
            var total = dict.Sum(x => x.Value);

            Console.WriteLine($" Your order total is:    ${total}");
            Console.WriteLine("                                  ");

            Console.WriteLine("Please enter the cash amount you are payng with today");
            Console.WriteLine(" ");
            var cash = double.Parse(Console.ReadLine());

            if (cash >= total)
            {
                Console.WriteLine($"Thank you , your change is ${cash - total}");
                Console.ReadLine();
            }
            else if (cash <= total)
            {
                Console.WriteLine($"Sorry you are ${total - cash} short");
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine("Sorry, invalid amount");
            }
        }
        public static void drinks()
        {

            Console.WriteLine($" Which choice of drink would you like to add to your order?");

            double pepsi = 4.50;
            double coke = 5.00;
            double beer = 7.00;
            double mount = 4.50;
            double redbull = 5.50;
            Console.WriteLine("");
            Console.WriteLine("________Drinks menu________");
            Console.WriteLine($" Pepsi: ${pepsi}");
            Console.WriteLine($" Coke:${coke}");
            Console.WriteLine($" Beer: ${beer}");
            Console.WriteLine($" Mountain Dew: ${mount}");
            Console.WriteLine($" redbull: ${redbull}");


            var drink = Console.ReadLine();
            switch (drink)
            {
                case "pepsi":
                    Console.Clear();
                    Console.WriteLine($"You have selected one {pepsi}");
                    dict.Add(drink, pepsi);

                    break;

                case "coke":
                    Console.Clear();
                    Console.WriteLine($"You have selected one {coke}");
                    dict.Add(drink, coke);

                    break;

                case "beer":
                    Console.Clear();
                    Console.WriteLine($"You have selected one {drink} ");
                    dict.Add(drink, beer);

                    break;
                case "mountain dew":
                    Console.Clear();
                    Console.WriteLine($"You have selected one {drink} ");
                    dict.Add(drink, mount);

                    break;
                case "redbull":
                    Console.Clear();
                    Console.WriteLine($"You have selected one {drink} ");
                    dict.Add(drink, redbull);

                    break;
                default:
                    Console.WriteLine("The selection was invalid");

                    dict.Clear();

                    break;
            }
        }



    }
}